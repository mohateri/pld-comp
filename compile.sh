#!/bin/sh
dos2unix compile.sh
# sh script to compile some c file with the ifcc compiler

# create the command to be executed
command="compiler/ifcc"

for var in "$@"
do
    command="$command $var"
done

NAME=$1
NAME=${NAME%?}
NAME=${NAME%?}

# execute the command
command="$command >${NAME}.s"
eval $command

# execute gcc to produce .exe file
gcc ${NAME}.s -o ${NAME}