#ifndef IR_H
#define IR_H

#include <vector>
#include <string>
#include <iostream>
#include <initializer_list>

// Declarations from the parser -- replace with your own
// #include "type.h"
#include "symbolTable.h"

class BasicBlock;
class CFG;
class Visitor;


//! The class for one 3-address instruction
class IRInstr {

   public:
	/** The instructions themselves -- feel free to subclass instead */
	typedef enum {
		label,
		jmp,
		jmpCond,
		ldconst,
		copy,
		add,
		sub,
		mul,
		div,
		rmem,
		wmem,
		call,
		cmp,
		cmp_lt,
		cmp_le,
		pushq,
		popq,
		ret,
		movlEAX,
	} Operation;

	/**  constructor */
	IRInstr(BasicBlock* bb, Operation op, string type, vector<string> params);

	/** Actual code generation */
	/**< x86 assembly code generation for this IR instruction */
	void gen_asm(ostream &o);

	/**< MSP430 assembly code generation for this IR instruction */
	void gen_msp(ostream &o);

 private:
	BasicBlock* bb; /**< The BB this instruction belongs to, which provides a pointer to the CFG this instruction belong to */
	Operation op;
	string type;
	vector<string> params; /**< For 3-op instrs: d, x, y; for ldconst: d, c;  For call: label, d, params;  for wmem and rmem: choose yourself */
	// if you subclass IRInstr, each IRInstr subclass has its parameters and the previous (very important) comment becomes useless: it would be a better design.
};






/**  The class for a basic block */

/* A few important comments.
	 IRInstr has no jump instructions.
	 cmp_* instructions behaves as an arithmetic two-operand instruction (add or mult),
	  returning a boolean value (as an int)

	 Assembly jumps are generated as follows:
	 BasicBlock::gen_asm() first calls IRInstr::gen_asm() on all its instructions, and then
		    if  exit_true  is a  nullptr,
            the epilogue is generated
        else if exit_false is a nullptr,
          an unconditional jmp to the exit_true branch is generated
				else (we have two successors, hence a branch)
          an instruction comparing the value of test_var_name to true is generated,
					followed by a conditional branch to the exit_false branch,
					followed by an unconditional branch to the exit_true branch
	 The attribute test_var_name itself is defined when converting
  the if, while, etc of the AST  to IR.

Possible optimization:
     a cmp_* comparison instructions, if it is the last instruction of its block,
       generates an actual assembly comparison
       followed by a conditional jump to the exit_false branch
*/

class BasicBlock {
 public:
	BasicBlock(CFG* cfg, string entry_label);
	void setTestVar(string testVar);

	/**< x86 assembly code generation for this basic block (very simple) */
	void gen_asm(ostream &o);
	void gen_msp(ostream &o);

	void add_IRInstr(IRInstr::Operation op, string type, vector<string> params);

	// No encapsulation whatsoever here. Feel free to do better.
	string jump;
	BasicBlock* exit_true;  /**< pointer to the next basic block, true branch. If nullptr, return from procedure */
	BasicBlock* exit_false; /**< pointer to the next basic block, false branch. If null_ptr, the basic block ends with an unconditional jump */
	string label; /**< label of the BB, also will be the label in the generated code */
	string blockType; /** is it a normal block, a then block or an else block */
	CFG* cfg; /** < the CFG where this block belongs */
	vector<IRInstr*> instrs; /** < the instructions themselves. */
  	string test_var_name;  /** < when generating IR code for an if(expr) or while(expr) etc,
													 store here the name of the variable that holds the value of expr */
    bool generated;

 protected:


};




/** The class for the control flow graph, also includes the symbol table */

/* A few important comments:
	 The entry block is the one with the same label as the AST function name.
	   (it could be the first of bbs, or it could be defined by an attribute value)
	 The exit block is the one with both exit pointers equal to nullptr.
     (again it could be identified in a more explicit way)

 */
class CFG {
 public:
	// CFG(Visitor* visitor);
	CFG();
	// Visitor* visitor; /**< The visitor this CFG comes from */

	void add_bb(BasicBlock* bb);

	// x86 code generation: could be encapsulated in a processor class in a retargetable compiler
	void gen_asm(ostream& o);
	string IR_reg_to_asm(string reg); /**< helper method: inputs a IR reg or input variable, returns e.g. "-24(%rbp)" for the proper value of 24 */
	void gen_asm_prologue(ostream& o);
	void gen_asm_epilogue(ostream& o);

	void gen_msp(ostream& o);
	void gen_msp_prologue(ostream& o);
	void gen_msp_epilogue(ostream& o);

	// symbol table methods
	void add_to_symbol_table(string varName, string type, bool used = true);
	string create_new_tempvar(string varName, string type);
	int get_var_index(string name);
	string get_var_type(string name);

	// basic block management
	string new_BB_name();
	BasicBlock* current_bb;

	void addInstruction(IRInstr::Operation op, string type, vector<string> params);

	SymbolTable* getSymbolTable();
	int nextBBnumber; /**< just for naming */
	bool hasReturn;

	string getName() { return bbs[0]->label; }

	int getNextFreeSymbolIndex() { return nextFreeSymbolIndex; }

	void setOffset(int o) { symbolTable->setOffset(o); }


 protected:
	// map <string, Type> SymbolType; /**< part of the symbol table  */
	// map <string, int> SymbolIndex; /**< part of the symbol table  */
	SymbolTable* symbolTable;
	int nextFreeSymbolIndex; /**< to allocate new symbols in the symbol table */

	vector <BasicBlock*> bbs; /**< all the basic blocks of this CFG*/
};

#endif
