#ifndef SYMBOLTABLE_H
#define SYMBOLTABLE_H

#pragma once

#include <iostream>
using namespace std;

// Credit : ShraddhaVarat
// Webstite : https://www.geeksforgeeks.org/cpp-program-to-implement-symbol-table/

const int MAX = 400;

class SymbolNode {

    string identifier, value, type;
    int lineNo;
    SymbolNode* next;
    bool used;

public:
    SymbolNode(){
        next = NULL;
    }

    SymbolNode(string key, string value, string type, bool used = true){
        this->identifier = key;
        this->value = value;
        this->type = type;
        next = NULL;
        this->used = used;
    }

    void print(){
        cout << "Identifier's Name:" << identifier
             << "Type:" << type
             << "Value: " << value<< endl;
    }

    void setUsed() { used = true; }

    string getType() { return type; }

    friend class SymbolTable;
};

class SymbolTable {
    SymbolNode* head[MAX];

public:
    SymbolTable(){
        for (int i = 0; i < MAX; i++)
            head[i] = NULL;
    }

    void insert(string id, string value, string Type, bool used = true);

    string find(string id);

    bool setValue(string id, string v);

    bool checkUtilisation(ostream& o);

    int getIndex(string id);

    void setOffset(int o) { offset = o; }

    void setUsed(string nodeName);

    string getType(string nodeName);

private:
    int offset = 0;
    int compteur = 1;
};

#endif
