#include "symbolTable.h"
#include <fstream>

using namespace std;

// Function to set the value of an identifier
bool SymbolTable::setValue(string id, string v){
    int index = 4;
    while (index < MAX) {
        if( head[index] == nullptr){
            return false;
        }else{
            if (head[index]->identifier == id) {
                head[index]->value = v;
                return true;
            }
            index += 4;
        }
    }

    return false; // id not found
}

// Function to find an identifier and return its value
string SymbolTable::find(string id){
    int index = 4;
    while (index < MAX) {
        if( head[index] == nullptr){
            return " ";
        }else{
            if ( head[index]->identifier == id) {
                return head[index]->value;
            }
            index += 4;
        }
    }

    return " "; // not found
}


// Function to find an identifier and return its value
int SymbolTable::getIndex(string id){
    int index = 4;
    while (index < MAX) {
        if( head[index] == nullptr){
            return -1;
        }else{
            if (head[index]->identifier == id) {
                return offset+index;
            }
            index += 4;
        }
    }
    return -1; // not found
}

// Function to insert an identifier
 void SymbolTable::insert(string id, string value, string Type, bool used){
    SymbolNode* p = new SymbolNode(id, value, Type, used);
    int index = compteur*4;
    head[index] = p;
    /*
    SymbolNode* start = head[index];
    while (start->next != NULL){
        start = start->next;
    }

    start->next = p;
    */
    compteur++;
}


void SymbolTable::setUsed(string nodeName) {
    int index = 4;
    while (index < MAX) {
        if( head[index] == nullptr){
            break;
        }else{
            if (head[index]->identifier == nodeName) {
                head[index]->setUsed();
                break;
            }
            index += 4;
        }
    }// not found

}

string SymbolTable::getType(string nodeName) {
    int index = 4;
    while (index < MAX) {
        if(head[index] == nullptr){
            return "";
        }else{
            if (head[index]->identifier == nodeName) {
                return head[index]->getType();
            }
            index += 4;
        }
    }
    return "";

}

bool SymbolTable::checkUtilisation(ostream& o){
    int index = 4;

    while (index < MAX) {
        if(head[index] != nullptr && head[index]->used == false) {
            o << "La variable " + head[index]->identifier +" n'a pas ete utilisee.\n";
        }
        index += 4;
    }

    return true;
}

