#include "IR.h"

//IR
/**  constructor */
IRInstr::IRInstr(BasicBlock* bb, Operation op, string type, vector<string> params){
	this->bb = bb;
	this->op = op;
	this->type = type;
	this->params = params;
}

/** Actual code generation */
/**< x86 assembly code generation for this IR instruction */
void IRInstr::gen_asm(ostream &o){
	switch(op){
		case ldconst:{
			o << "    movl    $" << params[1] << ", -" << params[0] << "(%rbp)\n";
		}break;
		case movlEAX:{
			o << "    movl    -" << params[0] << "(%rbp), " << "%eax" << "\n";
		}break;
		case copy:{
			o << "    movl    -" << params[1] << "(%rbp), " << "%eax\n";
			o << "    movl    %eax, -" << params[0] << "(%rbp)\n";
		}break;
		case add:{
			o << "    movl    -" << params[1] << "(%rbp), " << "%eax\n";
			o << "    addl    -" << params[2] << "(%rbp), " << "%eax\n";
			o << "    movl    %eax, -" << params[0] << "(%rbp)\n";
		}break;
		case sub:{
			o << "    movl    -" << params[1] << "(%rbp), " << "%eax\n";
			o << "    subl    -" << params[2] << "(%rbp), " << "%eax\n";
			o << "    movl    %eax, -" << params[0] << "(%rbp)\n";
		}break;
		case mul:{
			o << "    movl    -" << params[1] << "(%rbp), " << "%eax\n";
			o << "    imull    -" << params[2] << "(%rbp), " << "%eax\n";
			o << "    movl    %eax, -" << params[0] << "(%rbp)\n";
		}break;
		case div:{
			o << "    movl    -" << params[1] << "(%rbp), " << "%eax\n";
			o << "    cltd\n";
			o << "    idivl    -" << params[2] << "(%rbp)\n";
			o << "    movl    %eax, -" << params[0] << "(%rbp)\n";
		}break;
		case call:{
			o << "    call    " << params[0] << "@PLT\n";
			o <<"     movl	%eax, -" << params[1] << "(%rbp)\n";
			o << "    movl	$0, %eax\n";
		}break;
		case wmem:{
			o << "    movq    -" << params[1] <<"(%rbp), " << params[0] << "\n";
		}break;
		case cmp:{
			o << "    movl  -" << params[0]<<"(%rbp), %eax \n";
			o << "    cmpl  -"<< params[1]<<"(%rbp), %eax\n";
		}break;
		case label:{
			o << ".L" << params[0] << ":\n";
		}break;
		case jmpCond:{
			o << "    "<< params[0]<<"  "<< params[1] << "\n";
		}break;
		case jmp:{
			o << "    jmp  " << params[0] << "\n";
		}break;
		case rmem:{
			o << "    movq    " << params[1] <<", -" << params[0] << "(%rbp)\n";
		}break;
        default:{}
	}
}

void IRInstr::gen_msp(ostream &o){
	switch(op){
		case ldconst:{
			o << "    MOV.W #" << params[1] << ", -" << params[0] << "(R4)\n";
		}break;
		case movlEAX:{
			o << "    MOV.B -" << params[0] << "(R4), " << "R12" << "\n";
		}break;
		case copy:{
			o << "    MOV.W -" << params[1] << "(R4), -" << params[0] << "(R4)\n";
		}break;
		case add:{
			o << "    MOV.W -" << params[1] << "(R4), R12\n";
			o << "    ADD.W -" << params[2] << "(R4), R12\n";
			o << "    MOV.W R12, -" << params[0] << "(R4)\n";
		}break;
		case sub:{
			o << "    MOV.W -" << params[1] << "(R4), R12\n";
			o << "    SUB.W -" << params[2] << "(R4), R12\n";
			o << "    MOV.W R12, -" << params[0] << "(R4)\n";
		}break;
		case mul:{
			o << "    MOV.W -" << params[1] << "(R4), R13\n";
			o << "    MOV.W -" << params[2] << "(R4), R12\n";
			o << "    CALL #__mspabi_mpyi\n";
			o << "    MOV.W R12, -" << params[0] << "(R4)\n";
		}break;
		case div:{
			o << "    MOV.W -" << params[1] << "(R4), R13\n";
			o << "    MOV.W -" << params[2] << "(R4), R12\n";
			o << "    CALL #__mspabi_divi\n";
			o << "    MOV.W R12, -" << params[0] << "(R4)\n";
		}break;
		default:{}
	}
}


//BASICBLOCK
BasicBlock::BasicBlock(CFG* cfg, string entry_label){
	this->cfg = cfg;
	label = entry_label;
	test_var_name = "";
	blockType = "normal";
	jump = "";
	generated = false;
}

void BasicBlock::setTestVar(string testVar){
	this->test_var_name = testVar;
}

/**< x86 assembly code generation for this basic block (very simple) */
void BasicBlock::gen_asm(ostream &o){
	int cpt;

	for(cpt = 0; cpt<instrs.size() ; cpt++){
		instrs[cpt]->gen_asm(o);
	}
}

/**< MSP430 assembly code generation for this basic block (very simple) */
void BasicBlock::gen_msp(ostream &o){
	int cpt;

	for(cpt = 0; cpt<instrs.size() ; cpt++){
		instrs[cpt]->gen_msp(o);
	}
}

void BasicBlock::add_IRInstr(IRInstr::Operation op, string type, vector<string> params) {
	IRInstr* instr = new IRInstr(this, op, type, params);
	instrs.push_back(instr);
}


//CFG
CFG::CFG(){
	nextFreeSymbolIndex = 4; /**< to allocate new symbols in the symbol table */
	nextBBnumber = 0; /**< just for naming */
	this->symbolTable = new SymbolTable();
	hasReturn = false;
	//this->current_bb = new BasicBlock(this, to_string(nextBBnumber));
	//add_bb(current_bb);
}

void CFG::add_bb(BasicBlock* bb){
	bbs.push_back(bb);
	nextBBnumber++;
}

void CFG::gen_asm(ostream &o){
    /*
    vector <BasicBlock*> stackBB;
    BasicBlock *current = nullptr;
    stackBB.push_back(bbs[0]);
    while (!stackBB.empty()) {
        current = stackBB.back();
        //cout << current->label << " : : " <<endl;
        if(!current->generated) {

            if(current->test_var_name == "if"){
                string destination = ".L"+current->exit_false->label;
                vector<string> params;
                params.push_back(current->jump);
                params.push_back(destination);
                current->add_IRInstr(IRInstr::jmpCond, "", params);
            }else if(current->blockType == "then"){
                string destination = ".L"+current->exit_true->label;
                vector<string> params;
                params.push_back(destination);
                current->add_IRInstr(IRInstr::jmp, "", params);
            }else if(current->test_var_name == "while"){
                string destination = ".L"+current->exit_false->label;
                vector<string> params;
                params.push_back(current->jump);
                params.push_back(destination);
                current->add_IRInstr(IRInstr::jmpCond, "", params);
            }
            current->gen_asm(o);
        }
        current->generated = true;
        if(current->exit_true == nullptr || current->exit_true->generated) {
            if(current->exit_false == nullptr || current->exit_false->generated ) {
                stackBB.pop_back();
            } else {
                stackBB.push_back(current->exit_false);
                current = current->exit_false;
            }
        } else {
            stackBB.push_back(current->exit_true);
            current = current->exit_true;
        }



    }
*/

	for(int cpt = 0; cpt<bbs.size() ; cpt++){
		if(bbs[cpt]->test_var_name == "if"){
			string destination = ".L"+bbs[cpt]->exit_false->label;
			vector<string> params;
			params.push_back(bbs[cpt]->jump);
			params.push_back(destination);
			bbs[cpt]->add_IRInstr(IRInstr::jmpCond, "", params);
		}
		/*else if(bbs[cpt]->blockType == "then"){
			string destination = ".L"+bbs[cpt]->exit_true->label;
			vector<string> params;
			params.push_back(destination);
			bbs[cpt]->add_IRInstr(IRInstr::jmp, "", params);
		}*/
		else if(bbs[cpt]->test_var_name == "while"){
			string destination = ".L"+bbs[cpt]->exit_false->label;
			vector<string> params;
			params.push_back(bbs[cpt]->jump);
			params.push_back(destination);
			bbs[cpt]->add_IRInstr(IRInstr::jmpCond, "", params);
		}
		//cout << endl << bbs[cpt]->label << endl << endl;
		bbs[cpt]->gen_asm(o);
	}

}

// symbol table methods
void CFG::add_to_symbol_table(string varName, string type, bool used) {
    symbolTable->insert(varName, "",  type, used);
}

string CFG::create_new_tempvar(string varName, string type){
	string nomVariable;
	if(varName == ""){
    	nomVariable = "!tmp" + to_string(nextFreeSymbolIndex);
	}else{
		nomVariable = varName;
	}

	//A changer pour d'autres types
    if(type=="int"){
        nextFreeSymbolIndex += 4;
    }else if(type == "char"){
		nextFreeSymbolIndex += 4;
	}else{
		nextFreeSymbolIndex += 4;
	}
    return nomVariable;
}

void CFG::addInstruction(IRInstr::Operation op, string type, vector<string> params) {
	// String nomTempVar = create_new_tempvar(type);
	// add_to_symbol_table(nomTempVar, type);
	current_bb->add_IRInstr(op, type, params);
}

SymbolTable* CFG::getSymbolTable(){
	return this->symbolTable;
}

void CFG::gen_asm_prologue(ostream& o){

	int free_space = nextFreeSymbolIndex;
	// On prend le mulitple de 8 >=
	if(free_space%8 != 0) free_space = ((int)nextFreeSymbolIndex/8 + 1)*8;

    o << bbs[0]->label << ":" << endl;
	o << "    pushq    %rbp" << endl;
	o << "    movq	   %rsp, %rbp" << endl;
	o << "    subq	   $"<< free_space <<", %rsp " << endl;
}

void CFG::gen_asm_epilogue(ostream& o){

	o << "    leave\n";
	// o << "    popq	   %rbp" << endl;
	o << "    ret" << endl;
}

void CFG::gen_msp_prologue(ostream& o){
	o << bbs[0]->label << ":" << endl;
	o << "    PUSHM.W #1, R4" << endl;
	o << "    MOV.W   R1, R4" << endl;
}

void CFG::gen_msp(ostream &o){
	/*BasicBlock* bb=bbs[0];

	while(bb->exit_true != null_ptr && bb->exit_false != null_ptr){
		bb->gen_asm(o);
		bb = ;
	}*/
	int cpt;

	for(cpt = 0; cpt<bbs.size() ; cpt++){
		bbs[cpt]->gen_msp(o);
	}
}

void CFG::gen_msp_epilogue(ostream& o){
	o << "    POPM.W  #1, r4" << endl;
    o << "    RET" << endl;
}
