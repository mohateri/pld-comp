grammar ifcc;

axiom : prog
      ;

prog : functions main functions;

main : 'int' 'main' '(' ')' '{' instruction+ '}';

function : ('int'|'void') IDENT '(' parametre0 ')' '{' instruction* '}'
     ;

functions : function*;

parametre0 : 'int' IDENT parametre1* #someParameters0
           | #noParameters0
           ;

parametre1 : ',' 'int' IDENT
     ;

vars : ',' IDENT vars #newVar
     | #noMoreVar
     ;

instruction : expr ';' #exprInstr
            | IDENT '=' expr ';' #affectInstr
            | 'return' expr ';' #retourInstr
            | 'int' IDENT vars ';' #declarInstr
            | 'int' IDENT '=' expr ';' #declarAndAffectInstr
            | 'if' '(' compare ')' '{' instruction+ '}' elseRule #ifClause
            | 'while' '(' compare ')' '{' instruction+ '}' #whileClause
            ;

compare : expr SIGN expr #compareClause
        ;

elseRule : 'else' '{' instruction+ '}' #elseClause
     | #noElse
     ;

constRule : QUOTE IDENT QUOTE #constChar
     | CONST #constInt
     ;

expr : '(' expr ')' #parentheses
     | expr '*' expr #multiplication
     | expr '/' expr #division
     | expr '+' expr #addition
     | expr '-' expr #soustraction
     | IDENT '(' param0 ')' #appel
     | IDENT #var
     | constRule #const
     ;

param0 : expr param1 #param
       | #noMoreParam0
       ;
param1 : ',' expr param1 #newParam
       | #noMoreParam1
       ;

IDENT : [a-zA-Z][a-zA-Z0-9]* ;
CONST : [0-9]+ ;
SIGN : '<' | '<=' | '==' | '>=' | '>' | '!=' ;
COMMENT : '/*' .*? '*/' -> skip ;
QUOTE : '\'' ;
CHAR : '*' ;
DIRECTIVE : '#' .*? '\n' -> skip ;
WS    : [ \t\r\n] -> channel(HIDDEN);
