#ifndef VISITOR_H
#define VISITOR_H

#pragma once

#include <string.h>
#include <typeinfo>
#include "antlr4-runtime.h"
#include "antlr4-generated/ifccVisitor.h"
#include "IR.h"

using namespace std;
/**
 * This class provides an empty implementation of ifccVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  Visitor : public ifccVisitor {

public:

  Visitor(vector<string> v);

  // AXIOM
  virtual antlrcpp::Any visitAxiom(ifccParser::AxiomContext *ctx) override;

  // PROG
  virtual antlrcpp::Any visitProg(ifccParser::ProgContext *ctx) override;

  virtual antlrcpp::Any visitFunctions(ifccParser::FunctionsContext *ctx) override;

  // EXPR
  virtual antlrcpp::Any visitParentheses(ifccParser::ParenthesesContext *ctx) override;

  virtual antlrcpp::Any visitMultiplication(ifccParser::MultiplicationContext *ctx) override;

  virtual antlrcpp::Any visitDivision(ifccParser::DivisionContext *ctx) override;

  virtual antlrcpp::Any visitAddition(ifccParser::AdditionContext *ctx) override;

  virtual antlrcpp::Any visitSoustraction(ifccParser::SoustractionContext *ctx) override;

  virtual antlrcpp::Any visitConstInt(ifccParser::ConstIntContext *ctx) override;

  virtual antlrcpp::Any visitConstChar(ifccParser::ConstCharContext *ctx) override;

  virtual antlrcpp::Any visitConst(ifccParser::ConstContext *ctx) override;

  // VARS
  virtual antlrcpp::Any visitVar(ifccParser::VarContext *ctx) override;

  virtual antlrcpp::Any visitNewVar(ifccParser::NewVarContext *ctx) override;

  virtual antlrcpp::Any visitNoMoreVar(ifccParser::NoMoreVarContext *ctx) override;

  // INSTRUCTION
  virtual antlrcpp::Any visitExprInstr(ifccParser::ExprInstrContext *ctx) override;

  virtual antlrcpp::Any visitAffectInstr(ifccParser::AffectInstrContext *ctx) override;

  virtual antlrcpp::Any visitDeclarInstr(ifccParser::DeclarInstrContext *ctx) override;

  virtual antlrcpp::Any visitRetourInstr(ifccParser::RetourInstrContext *ctx) override;

  virtual antlrcpp::Any visitDeclarAndAffectInstr(ifccParser::DeclarAndAffectInstrContext *ctx) override;

  virtual antlrcpp::Any visitIfClause(ifccParser::IfClauseContext *ctx) override;

  virtual antlrcpp::Any visitWhileClause(ifccParser::WhileClauseContext *ctx) override;

  // IF
  virtual antlrcpp::Any visitCompareClause(ifccParser::CompareClauseContext *ctx) override;

  virtual antlrcpp::Any visitElseClause(ifccParser::ElseClauseContext *ctx) override;

  virtual antlrcpp::Any visitNoElse(ifccParser::NoElseContext *ctx) override;

  // APPEL
  virtual antlrcpp::Any visitAppel(ifccParser::AppelContext *ctx) override;

  virtual antlrcpp::Any visitParam(ifccParser::ParamContext *ctx) override;

  virtual antlrcpp::Any visitNewParam(ifccParser::NewParamContext *ctx) override;

  virtual antlrcpp::Any visitNoMoreParam0(ifccParser::NoMoreParam0Context *ctx) override;

  virtual antlrcpp::Any visitNoMoreParam1(ifccParser::NoMoreParam1Context *ctx) override;

  virtual antlrcpp::Any visitFunction(ifccParser::FunctionContext *ctx) override;

  virtual antlrcpp::Any visitSomeParameters0(ifccParser::SomeParameters0Context *ctx) override;

  virtual antlrcpp::Any visitNoParameters0(ifccParser::NoParameters0Context *ctx) override;

  virtual antlrcpp::Any visitParametre1(ifccParser::Parametre1Context *ctx) override;

  virtual antlrcpp::Any visitMain(ifccParser::MainContext *ctx) override;

private:
  CFG* cfg;
  bool isOk;
  bool gen_asm;
  bool gen_msp;
  int cpt;
  ofstream myFile;
  ofstream errFile;
  int cptRegistre;

  string libFile;

  SymbolTable *globalSymbolTable;
};


#endif // VISITOR_H

