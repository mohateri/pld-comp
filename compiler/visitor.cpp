
// Generated from ifcc.g4 by ANTLR 4.7.2

#include "visitor.h"

Visitor::Visitor(vector<string> v){
    this->cfg = nullptr;
    this -> globalSymbolTable = new SymbolTable();
    isOk = true;
    cpt = 0;
    gen_msp = false;
    gen_asm = false;
    libFile = "compiler/lib_stdio.txt";

    bool fileSpecified = false;
    for(string s : v){
        if (fileSpecified) {
            fileSpecified = false;
            libFile = s;
        }
        if(s=="-m"){
            gen_msp = true;
        } else if (s=="-a") {
            gen_asm = true;
            cout << ".globl main" << endl;
        } else if (s=="-l") {
            fileSpecified = true;
        }
    }
}

// AXIOM

antlrcpp::Any Visitor::visitAxiom(ifccParser::AxiomContext *ctx) {
    errFile.open("errors.txt");
    int res = visit(ctx->prog());
    /*
    if(gen_asm && res==0){
        cfg->gen_asm_prologue(cout);
        cfg->gen_asm(cout);
        cfg->gen_asm_epilogue(cout);
    }

    if(gen_msp && res==0){
        myFile.open("msp430-pld.s");

        cfg->gen_msp_prologue(myFile);
        cfg->gen_msp(myFile);
        cfg->gen_msp_epilogue(myFile);

        myFile.close();
    }
    cfg->getSymbolTable()->checkUtilisation(errFile);
    */
    errFile.close();
    return res;
}

// PROG

antlrcpp::Any Visitor::visitProg(ifccParser::ProgContext *ctx) {
    bool resVisit;

    string funcName;
    string type;
    ifstream stdlib(libFile);
    if (!stdlib) {
        errFile << "Couldn't open stdlib file, check path." << endl;
        return 1;
    }
    while (getline(stdlib, funcName))
    {
        //cout << funcName << endl;
        globalSymbolTable->insert(funcName, "", "int", true);
        // process pair (a,b)
    }
    stdlib.close();


    globalSymbolTable->insert("main", "", "int", true);
    //globalSymbolTable->insert("putchar", "", "int");

    for(auto func: ctx->functions(0)->function() ){
        funcName = func->IDENT()->getText();
        type = func->getStart()->getText();
        if (globalSymbolTable->find(funcName) == " ") {
            globalSymbolTable->insert(funcName, "", type);
        }
        else {
            errFile << "Function : " << funcName << " defined twice.\n";
            return 1;
        }
    }


    for(auto func: ctx->functions(1)->function() ){
        funcName = func->IDENT()->getText();
        if (globalSymbolTable->find(funcName) == " ") {
            globalSymbolTable->insert(funcName, "", "int");
        }
        else {
            errFile << "Function : " << funcName << " defined twice.\n";
            return 1;
        }
    }


    resVisit = visit(ctx->main());
    resVisit = resVisit && visit(ctx->functions(0));
    resVisit = resVisit && visit(ctx->functions(1));
    if(!resVisit){
        errFile << "Error in visit." << endl;
        return 1;
    }
    return 0;
}

antlrcpp::Any Visitor::visitFunctions(ifccParser::FunctionsContext *ctx) {
    bool resVisit;
    for(auto func: ctx->function() ){
        resVisit = visit(func);
        if(!resVisit){
            errFile << "Error: bad function" << endl;
            return false;
        }
    }
    return true;
}
// EXPR

antlrcpp::Any Visitor::visitParentheses(ifccParser::ParenthesesContext *ctx) {
    return visit(ctx->expr());
}

antlrcpp::Any Visitor::visitMultiplication(ifccParser::MultiplicationContext *ctx) {
    string varName = "";
    string vst0 = visit(ctx->expr(0));
    string vst1 = visit(ctx->expr(1));

    if (vst0 == "_VOID" || vst1 == "_VOID") {
        errFile << "Void function used as int value" << endl;
        return "";
    }

    if(vst0 != "" && vst1 != ""){
        int pos0 = cfg->getSymbolTable()->getIndex(vst0);
        int pos1 = cfg->getSymbolTable()->getIndex(vst1);

        varName = cfg->create_new_tempvar("", "int");
        cfg->add_to_symbol_table(varName, "int");

        SymbolTable* st = cfg->getSymbolTable();

        vector<string> params;
        params.push_back(to_string(st->getIndex(varName)));
        params.push_back(to_string(pos0));
        params.push_back(to_string(pos1));
        cfg->addInstruction(IRInstr::mul, "int", params);

        //st->setValue(varName,to_string(stoi(st->find(vst0))*stoi(st->find(vst1))));
    }

    return varName;
}

antlrcpp::Any Visitor::visitDivision(ifccParser::DivisionContext *ctx) {
    string varName = "";
    string vst0 = visit(ctx->expr(0));
    string vst1 = visit(ctx->expr(1));

    if (vst0 == "_VOID" || vst1 == "_VOID") {
        errFile << "Void function used as int value" << endl;
        return "";
    }

    if(vst0 != "" && vst1 != ""){
        int pos0 = cfg->getSymbolTable()->getIndex(vst0);
        int pos1 = cfg->getSymbolTable()->getIndex(vst1);
        varName = cfg->create_new_tempvar("", "int");
        cfg->add_to_symbol_table(varName, "int");
        vector<string> params;

        SymbolTable* st = cfg->getSymbolTable();
        params.push_back(to_string(st->getIndex(varName)));
        params.push_back(to_string(pos0));
        params.push_back(to_string(pos1));
        cfg->addInstruction(IRInstr::div, "int", params);
        /*
        if(stoi(st->find(vst1)) != 0){
            st->setValue(varName,to_string(stoi(st->find(vst0))/stoi(st->find(vst1))));
        }
        */

    }
    return varName;
}

antlrcpp::Any Visitor::visitAddition(ifccParser::AdditionContext *ctx) {
    string varName = "";
    string vst0 = visit(ctx->expr(0));
    string vst1 = visit(ctx->expr(1));

    if (vst0 == "_VOID" || vst1 == "_VOID") {
        errFile << "Void function used as int value" << endl;
        return "";
    }

    if(vst0 != "" && vst1 != ""){
        int pos0 = cfg->getSymbolTable()->getIndex(vst0);
        int pos1 = cfg->getSymbolTable()->getIndex(vst1);

        varName = cfg->create_new_tempvar("", "int");
        cfg->add_to_symbol_table(varName, "int");
        vector<string> params;

        SymbolTable* st = cfg->getSymbolTable();
        params.push_back(to_string(st->getIndex(varName)));
        params.push_back(to_string(pos0));
        params.push_back(to_string(pos1));
        cfg->addInstruction(IRInstr::add, "int", params);

        //st->setValue(varName,to_string(stoi(st->find(vst0))+stoi(st->find(vst1))));
    }

    return varName;
}

antlrcpp::Any Visitor::visitSoustraction(ifccParser::SoustractionContext *ctx) {
    string varName = "";
    string vst0 = visit(ctx->expr(0));
    string vst1 = visit(ctx->expr(1));

    if (vst0 == "_VOID" || vst1 == "_VOID") {
        errFile << "Void function used as int value" << endl;
        return "";
    }

    if(vst0 != "" && vst1 != ""){
        int pos0 = cfg->getSymbolTable()->getIndex(vst0);
        int pos1 = cfg->getSymbolTable()->getIndex(vst1);

        varName = cfg->create_new_tempvar("", "int");
        cfg->add_to_symbol_table(varName, "int");
        vector<string> params;

        SymbolTable* st = cfg->getSymbolTable();
        params.push_back(to_string(st->getIndex(varName)));
        params.push_back(to_string(pos0));
        params.push_back(to_string(pos1));
        cfg->addInstruction(IRInstr::sub, "int", params);

        //st->setValue(varName,to_string(stoi(st->find(vst0))-stoi(st->find(vst1))));
    }

    return varName;
}

antlrcpp::Any Visitor::visitConstInt(ifccParser::ConstIntContext *ctx) {
    vector<string> params;
    string nomTempVar = cfg->create_new_tempvar("", "int");
    cfg->add_to_symbol_table(nomTempVar, "int");
    int pos = cfg->getSymbolTable()->getIndex(nomTempVar);
    //cout << pos << endl;
    params.push_back(to_string(pos));
    params.push_back(ctx->CONST()->getText());
    cfg->getSymbolTable()->setValue(nomTempVar, ctx->CONST()->getText());
    cfg->addInstruction(IRInstr::ldconst, "int", params);   cfg->getSymbolTable()->setValue(nomTempVar, ctx->CONST()->getText());
    return nomTempVar;
}

antlrcpp::Any Visitor::visitConstChar(ifccParser::ConstCharContext *ctx){
    vector<string> params;
    string nomTempVar = cfg->create_new_tempvar("", "char");
    cfg->add_to_symbol_table(nomTempVar, "char");
    int pos = cfg->getSymbolTable()->getIndex(nomTempVar);
    params.push_back(to_string(pos));
    string caractere = ctx->IDENT()->getText();
    int posASCII = 0;
    if(caractere != ""){
        posASCII = (int)caractere.at(0);
    }else{
        errFile << "Error: empty character" << endl;
    }
    params.push_back(to_string(posASCII));
    cfg->addInstruction(IRInstr::ldconst, "char", params);

    cfg->getSymbolTable()->setValue(nomTempVar, caractere);
    return nomTempVar;
}


antlrcpp::Any Visitor::visitVar(ifccParser::VarContext *ctx) {
    string varName = ctx->IDENT()->getText();
    if (cfg->getSymbolTable()->find(varName) == " "){
        isOk = false;
        // ERROR
        // on cherche à trouver la valeur d'une variable qui n'a pas été déclarée
        return "";
    } else {
        cfg->getSymbolTable()->setUsed(varName);
        return varName;
    }
}

antlrcpp::Any Visitor::visitNewVar(ifccParser::NewVarContext *ctx) {

    //TODO : verifier que variable n est pas deja declaree
    string varName = ctx->IDENT()->getText();
    string nomTempVar = cfg->create_new_tempvar(varName, "int");
    cfg->add_to_symbol_table(nomTempVar, "int");
    return (bool)visit(ctx->vars());
}


antlrcpp::Any Visitor::visitNoMoreVar(ifccParser::NoMoreVarContext *ctx) {
    return (bool)true;
}

antlrcpp::Any Visitor::visitConst(ifccParser::ConstContext *ctx){
    return visit(ctx->constRule());
}


// INSTRUCTION

antlrcpp::Any Visitor::visitExprInstr(ifccParser::ExprInstrContext *ctx){
    string varName = visit(ctx->expr());
    if(varName == ""){
        errFile << "Error: bad expression" << endl;
        return false;
    }
    return true;
}

antlrcpp::Any Visitor::visitAffectInstr(ifccParser::AffectInstrContext *ctx) {
    string leftVarName = ctx->IDENT()->getText();
    isOk = true;
    string rightVarName = visit(ctx->expr());

    if (rightVarName == "_VOID") {
        errFile << "Void function used as rvalue" << endl;
        return false;
    }

    vector<string> params;

    if (isOk) {

        int lPos = -1;
        lPos = cfg->getSymbolTable()->getIndex(leftVarName);
        int rPos = -1;
        rPos = cfg->getSymbolTable()->getIndex(rightVarName);
        if( lPos != -1 && rPos != -1) {
            params.push_back(to_string(lPos));
            params.push_back(to_string(rPos));
            cfg->getSymbolTable()->setValue(leftVarName, cfg->getSymbolTable()->find(rightVarName));
            cfg->addInstruction(IRInstr::copy, "int", params);
            return true;
        } else {
            return false;
        }
    }
    else {
        cout << "Uses an undeclared variable.\n";
        return false;
    }
}

antlrcpp::Any Visitor::visitDeclarInstr(ifccParser::DeclarInstrContext *ctx) {
    string varName = ctx->IDENT()->getText();
    if (cfg->getSymbolTable()->find(varName) == " ") {
        string nomTempVar = cfg->create_new_tempvar(varName, "int");
        cfg->add_to_symbol_table(nomTempVar, "int", false);
        visit(ctx->vars());
    } else {
        return false;
    }
    return true;
}

antlrcpp::Any Visitor::visitRetourInstr(ifccParser::RetourInstrContext *ctx) {

    if (globalSymbolTable->getType(cfg->getName()) == "void") {
        errFile << "Warning : Return found in a void function : " << cfg->getName() << endl;
        return true;
    }
    cfg->hasReturn = true;
    vector<string> params;
    isOk = true;
    string tempVarName = visit(ctx->expr());
    int pos = cfg->getSymbolTable()->getIndex(tempVarName);
    if (pos == -1) {
        return false;
    }
    params.push_back(to_string(pos));

    if (isOk) {
        cfg->addInstruction(IRInstr::movlEAX, "int", params);
        return true;
    }
    else {
        errFile<<"Error: is not ok"<<endl;
        return false;
    }
}

antlrcpp::Any Visitor::visitDeclarAndAffectInstr(ifccParser::DeclarAndAffectInstrContext *ctx) {
    string varName = ctx->IDENT()->getText();
    if (cfg->getSymbolTable()->find(varName) == " ") {
        string nomTempVar = cfg->create_new_tempvar(varName, "int");
        cfg->add_to_symbol_table(nomTempVar, "int");

        isOk = true;
        string rightVarName = visit(ctx->expr());

        if (rightVarName == "_VOID") {
            errFile << "Void function used as rvalue" << endl;
            return false;
        }
        vector<string> params;

        if (isOk) {

            int lPos = -1;
            lPos = cfg->getSymbolTable()->getIndex(varName);
            int rPos = -1;
            rPos = cfg->getSymbolTable()->getIndex(rightVarName);
            if( lPos != -1 && rPos != -1) {
                params.push_back(to_string(lPos));
                params.push_back(to_string(rPos));
                cfg->getSymbolTable()->setValue(varName, cfg->getSymbolTable()->find(rightVarName));
                cfg->addInstruction(IRInstr::copy, "int", params);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    } else {
        errFile << "Redeclaration of variable " << varName << endl;
        return false;
    }
}

antlrcpp::Any Visitor::visitIfClause(ifccParser::IfClauseContext *ctx){
    BasicBlock* ifBB = new BasicBlock(cfg, "entry_if_"+ to_string(cfg->nextBBnumber));
    cfg->add_bb(ifBB);
    cfg->current_bb->test_var_name = "block";
    //cfg->current_bb->exit_true = ifBB;
    cfg->current_bb = ifBB;
    if(visit(ctx->compare())){
        cfg->current_bb->test_var_name = "if";
        BasicBlock* thenBB = new BasicBlock(cfg, "entry_then_"+ to_string(cfg->nextBBnumber));
        thenBB->blockType = "then";
        BasicBlock* elseBB = new BasicBlock(cfg, "entry_else_"+ to_string(cfg->nextBBnumber));
        elseBB->blockType = "else";
        BasicBlock* afterIfBB = new BasicBlock(cfg, "exit_if_"+ to_string(cfg->nextBBnumber));

        cfg->current_bb->exit_true = thenBB;
        cfg->current_bb->exit_false = elseBB;
        thenBB->exit_true = afterIfBB;
        thenBB->exit_false = NULL;
        elseBB->exit_true = afterIfBB;
        elseBB->exit_false = NULL;


        cfg->add_bb(thenBB);
        cfg->current_bb = thenBB;
        for(auto instr: ctx->instruction() ){
            bool resVisit = visit(instr);
            if(!resVisit){
                errFile << "Error: bad instruction" << endl;
                return false;
            }
        }

        BasicBlock *jumper = new BasicBlock(cfg, "jumper_"+to_string(cfg->nextBBnumber));
        cfg->add_bb(jumper);
        cfg->current_bb = jumper;
        string destination = ".L"+thenBB->exit_true->label;
        vector<string> params;
        params.push_back(destination);
        cfg->addInstruction(IRInstr::jmp, "", params);

        cfg->add_bb(elseBB);
        cfg->current_bb = elseBB;
        vector<string> params1;
        params1.push_back(elseBB->label);
        cfg->current_bb->add_IRInstr(IRInstr::label, "", params1);
        bool elseResult = visit(ctx->elseRule());
        if(elseResult == false){
            errFile<<"Error: problem in else" << endl;
            return false;
        }
        cfg->add_bb(afterIfBB);
        cfg->current_bb = afterIfBB;
        vector<string> params2;
        params2.push_back(afterIfBB->label);
        cfg->current_bb->add_IRInstr(IRInstr::label, "", params2);
    }
    return true;
}

antlrcpp::Any Visitor::visitWhileClause(ifccParser::WhileClauseContext *ctx){
    BasicBlock* whileBB = new BasicBlock(cfg, "entry_while_"+ to_string(cfg->nextBBnumber));
    cfg->add_bb(whileBB);
    cfg->current_bb->test_var_name = "block";
    cfg->current_bb->exit_true = whileBB;
    cfg->current_bb = whileBB;
    vector<string> params;
    params.push_back(whileBB->label);
    cfg->current_bb->add_IRInstr(IRInstr::label, "", params);
    if(visit(ctx->compare())){
        cfg->current_bb->test_var_name = "while";
        BasicBlock* thenBB = new BasicBlock(cfg, "entry_then_"+ to_string(cfg->nextBBnumber));
        thenBB->blockType = "then";
        BasicBlock* afterWhileBB = new BasicBlock(cfg, "exit_while_"+ to_string(cfg->nextBBnumber));

        cfg->current_bb->exit_true = thenBB;
        cfg->current_bb->exit_false = afterWhileBB;
        thenBB->exit_true = whileBB;
        thenBB->exit_false = NULL;

        cfg->add_bb(thenBB);
        cfg->current_bb = thenBB;
        for(auto instr: ctx->instruction() ){
            bool resVisit = visit(instr);
            if(!resVisit){
                errFile << "Error: bad instruction" << endl;
                return false;
            }
        }

        BasicBlock *jumper = new BasicBlock(cfg, "jumper_"+to_string(cfg->nextBBnumber));
        cfg->add_bb(jumper);
        cfg->current_bb = jumper;
        string destination = ".L"+thenBB->exit_true->label;
        vector<string> params;
        params.push_back(destination);
        cfg->addInstruction(IRInstr::jmp, "", params);

        cfg->add_bb(afterWhileBB);

        cfg->current_bb = afterWhileBB;
        vector<string> params2;
        params2.push_back(afterWhileBB->label);
        cfg->current_bb->add_IRInstr(IRInstr::label, "", params2);
    }
    return true;
}

// IF

antlrcpp::Any Visitor::visitCompareClause(ifccParser::CompareClauseContext *ctx){
    string sign = ctx->SIGN()->getText();
    string nameExpr0 = visit(ctx->expr(0));
    string nameExpr1 = visit(ctx->expr(1));
    int pos0 = cfg->getSymbolTable()->getIndex(nameExpr0);
    int pos1 = cfg->getSymbolTable()->getIndex(nameExpr1);

    vector<string> params;
    params.push_back(to_string(pos0));
    params.push_back(to_string(pos1));
    cfg->addInstruction(IRInstr::cmp, "", params);
    if(sign == "<"){
        cfg->current_bb->jump = "jge";
    }else if(sign == "<="){
        cfg->current_bb->jump = "jg";
    }else if(sign == "=="){
        cfg->current_bb->jump = "jne";
    }else if(sign == ">="){
        cfg->current_bb->jump = "jl";
    }else if(sign == ">"){
        cfg->current_bb->jump = "jle";
    }else if(sign == "!="){
        cfg->current_bb->jump = "je";
    }else{
        errFile<< "Error: comparing failed" << endl;
        return false;
    }
    return true;
}

antlrcpp::Any Visitor::visitElseClause(ifccParser::ElseClauseContext *ctx){
    for(auto instr: ctx->instruction() ){
        bool resVisit = visit(instr);
        if(!resVisit){
            errFile << "Error: bad instruction" << endl;
            return false;
        }
    }
    return true;
}

antlrcpp::Any Visitor::visitNoElse(ifccParser::NoElseContext *ctx){
    return (bool)true;
}

// APPEL

antlrcpp::Any Visitor::visitAppel(ifccParser::AppelContext *ctx){
    string returnString;
    string funcName = ctx->IDENT()->getText();

    if (globalSymbolTable->getType(funcName) == "void") {
            returnString = "_VOID";
            return returnString;
    }
    if (globalSymbolTable->find(funcName) == " ") {
        errFile << "Use of undeclared function : " << funcName << endl;
        returnString = "";
        return returnString;
    }
    vector<string> params;
    params.push_back(funcName);
    if ((bool) visit(ctx->param0()) == true){
        returnString = cfg->create_new_tempvar("", "int");
        cfg->add_to_symbol_table(returnString, "int");
        params.push_back(to_string(cfg->getSymbolTable()->getIndex(returnString)));
        cfg->addInstruction(IRInstr::call, "int", params);
        if (globalSymbolTable->find(funcName) != " ")
            cfg->getSymbolTable()->setUsed(funcName);
    } else {
        returnString = "";
    }
    return returnString;
}

antlrcpp::Any Visitor::visitParam(ifccParser::ParamContext *ctx){
    vector<string> params;
    int pos = -1;
    cptRegistre = 0;

    string varName = visit(ctx->expr());
    pos = cfg->getSymbolTable()->getIndex(varName);
    if (pos != -1){
        params.push_back("%rdi");
        params.push_back(to_string(pos));
        cfg->addInstruction(IRInstr::wmem, "int", params);
        cptRegistre++;
        if(!(bool)visit(ctx->param1())){
            errFile<<"Error: too many arguments"<< endl;
            return false;
        }
    } else {
        errFile << "Error: argument not found" << endl;
        return false;
    }
    return true;
}

antlrcpp::Any Visitor::visitNewParam(ifccParser::NewParamContext *ctx){
    if(cptRegistre<=5){
        vector<string> regs = {"%rdi", "%rsi", "%rdx", "%rcx", "%r8", "%r9"};
        string varName = visit(ctx->expr());
        vector<string> params;
        int pos = -1;

        pos = cfg->getSymbolTable()->getIndex(varName);
        if(pos != -1){
            params.push_back(regs[cptRegistre]);
            params.push_back(to_string(pos));
            cptRegistre++;

            //Modifier selon le type du parametre
            cfg->addInstruction(IRInstr::wmem, "int", params);
            if(!(bool)visit(ctx->param1())){
                return false;
            }
        }else{
            return false;
        }
    }else{
        return false;
    }
    return true;
}

antlrcpp::Any Visitor::visitNoMoreParam0(ifccParser::NoMoreParam0Context *ctx){
    return (bool)true;
}

antlrcpp::Any Visitor::visitNoMoreParam1(ifccParser::NoMoreParam1Context *ctx){
    return (bool)true;
}

antlrcpp::Any Visitor::visitMain(ifccParser::MainContext *ctx){
    CFG *mainCFG = new CFG();
    BasicBlock *mainBB = new BasicBlock(mainCFG, "main");
    mainCFG->add_bb(mainBB);
    mainCFG->current_bb = mainBB;
    cfg = mainCFG;

    globalSymbolTable->setUsed("main");

    bool worked = true;

    for(auto instr: ctx->instruction() ){
        bool resVisit = visit(instr);
        if(!resVisit){
            errFile << "Error: bad instruction" << endl;
            worked = false;
            break;
        }
    }

    if(gen_asm && worked){
        cfg->gen_asm_prologue(cout);
        cfg->gen_asm(cout);
        cfg->gen_asm_epilogue(cout);
    }

    if(gen_msp && worked){
        myFile.open("msp430-pld.s");

        cfg->gen_msp_prologue(myFile);
        cfg->gen_msp(myFile);
        cfg->gen_msp_epilogue(myFile);

        myFile.close();
    }
    cfg->getSymbolTable()->checkUtilisation(errFile);
    globalSymbolTable->checkUtilisation(errFile);

    return worked;
}

antlrcpp::Any Visitor::visitFunction(ifccParser::FunctionContext *ctx){
    string varName = ctx->IDENT()->getText();
    int offset = cfg -> getNextFreeSymbolIndex() + 8;
    CFG *thisFuncCFG = new CFG();
    BasicBlock *thisFuncBB = new BasicBlock(thisFuncCFG, varName);
    thisFuncCFG->add_bb(thisFuncBB);
    thisFuncCFG->current_bb = thisFuncBB;
    thisFuncCFG->setOffset(offset);
    cfg = thisFuncCFG;


    bool worked = true;
    if(!(bool)visit(ctx->parametre0())) {
        errFile << "Error : Parameters\n";
        worked = false;
    }
    for(auto instr: ctx->instruction() ){
        bool resVisit = visit(instr);
        if(!resVisit){
            errFile << "Error: bad instruction" << endl;
            worked = false;
            break;
        }
    }
    cfg->getSymbolTable()->checkUtilisation(errFile);

    if(gen_asm && worked){
        cfg->gen_asm_prologue(cout);
        cfg->gen_asm(cout);
        cfg->gen_asm_epilogue(cout);
    }

    if(gen_msp && worked){
        myFile.open("msp430-pld.s");

        cfg->gen_msp_prologue(myFile);
        cfg->gen_msp(myFile);
        cfg->gen_msp_epilogue(myFile);

        myFile.close();
    }

    if (ctx->getStart()->getText() != "void" && !cfg->hasReturn) {
        errFile << "Warning : Non void function " << varName << " ends without return statement." << endl;
    }

    return worked;
}

antlrcpp::Any Visitor::visitSomeParameters0(ifccParser::SomeParameters0Context *ctx){
    cptRegistre = 0;
    vector<string> params;
    string paramName = ctx->IDENT()->getText();
    if (cfg->getSymbolTable()->find(paramName) == " ") {
        string param = cfg->create_new_tempvar(paramName, "int");
        cfg->add_to_symbol_table(param, "int");

        params.push_back(to_string(cfg->getSymbolTable()->getIndex(param)));
        params.push_back("%rdi");
        cfg->addInstruction(IRInstr::rmem, "int", params);
        cptRegistre++;
        for (auto param1 : ctx->parametre1()) {
            if (!(bool)visit(param1)) return false;
        }
    } else {
        return false;
    }
    return true;

}

antlrcpp::Any Visitor::visitNoParameters0(ifccParser::NoParameters0Context *ctx) {
    return true;
}

antlrcpp::Any Visitor::visitParametre1(ifccParser::Parametre1Context *ctx){
    if(cptRegistre<=5){
        vector<string> regs = {"%rdi", "%rsi", "%rdx", "%rcx", "%r8", "%r9"};
        string paramName = ctx->IDENT()->getText();
        if (cfg->getSymbolTable()->find(paramName) == " ") {
            string param = cfg->create_new_tempvar(paramName, "int");
            cfg->add_to_symbol_table(param, "int");

            vector<string> params;
            params.push_back(to_string(cfg->getSymbolTable()->getIndex(param)));
            params.push_back(regs[cptRegistre]);
            cfg->addInstruction(IRInstr::rmem, "int", params);
            cptRegistre++;
            return true;
        } else {
            return false;
        }
    }else{
        return false;
    }
}

