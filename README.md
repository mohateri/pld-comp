# PLD-Comp

Ce dépôt git contient l'ensemble des fichiers formant le projet compilateur de C de l'hexanôme H4244 lors de l'année scolaire 2019/2020.
Ce groupe est formé des membres suivants :
- Ahmed Abdel Rahman
- Augustin Bodet
- Robert Cinciuc
- Nicolas Neila
- Bruno Sader
- Brice Vachez
- Yucong Zhao

## Liste des fonctionnalités

Voici une liste des fonctionnalités du compilateur :
### Declaration et Definition
Le compilateur accepte les déclarations et définitions:
- de variables `int`
- de fonctions `void` et `int` (uniquement les définitions)

### Fonctions
De plus l'utilisation des fonctions de la librarie *stdio* de type int sont utilisables
ex : `putchar('a')` ou `getchar`

### Expressions
Une expression pour le compilateur peut :
- contenir des parenthèses 
- être une multiplication d'expressions
- être une division d'expressions (il est important de noter que la division par 0 est gérée comme dans *gcc* et donc ne bloque pas le compilateur)
- être une addition d'expressions
- être une soustraction d'expressions
- être un appel à une fonction
- être une variable
- être une constante `int` (les `char` sont transformés en `int`)

### Instructions
Une instruction valide pour le compilateur peut être :
- Une expression
- L'affectation d'une variable (par une expression)
- La declaration d'une variable
- La declaration et l'affectation d'une variable (ie. `int a = 1;`)
- Un bloc de contrôle
- Un retour

### Bloc de Contrôle
Le compilateur contient l'implémentation de deux blocs de contrôle, le `if-else` et le `while`.
Il est évident qu'un bloc de contrôle est constitué d'instructions et que la comparaison utilise des expressions.

### MSP430
Le compilateur ifcc présente aussi la possibilité de générer du code assembleur pour MSP430.
Cette génération est pourtant limitée aux fonctionnalités suivantes :
- Déclaration et affectation de variables
- Expressions arithmétiques

NB : Le compilateur ne requiert aucun ordre dans l'écriture du code (ie. `int a;` puis `a=1;` etc).

Vous trouverez ci-après deux manuels : le premier destiné aux utilisateurs du compilateurs, et le deuxième pensé pour des développeurs qui souhaiteraient contribuer au projet.

## Compiler le compilateur

En premier lieu, pour ce servir du compilateur, il va falloir le compiler.
Les instructions qui suivent sont pensées pour une machine sous Ubuntu, comme les ordinateurs de la salle de TP Linux de l'INSA de Lyon.
Il faut installer les paquets suivants :
- cmake
- antlr4
- uuid-dev
- pkg-config
- clang

Après cela, il faut installer `antlr4-cpp-runtime`.
Enfin, il va falloir modifier le `Makefile` du projet pour l'adapter à votre installation.
Les lignes à modifier sont les lignes 6, 7 et 8. Il faut respectivement indiquer les chemins vers antlr4, vers le dossier `runtime/src/` de `antlr4-cpp-runtime`, puis vers le dossier `dist/` de `antrl4-cpp-runtime`.

Pour lancer la compilation, il suffit de lancer le script `script-compile.sh` avec la commande :
```
sh script-compile.sh
```
Si votre machine possède plus de deux cœurs, vous pouvez modifier ce script et augmenter l'argument suivant `make -j`. Ce nombre représente le nombre de cœurs exécutant le make en parallèle, ce qui sera donc plus rapide.

## Manuel utilisateur

La ligne de commande permettant d'exécuter le compilateur est la suivante :
```
sh compile.sh prog_source -a [-m] [-l stdlib_file]
```

- `prog_source` est un programme en C, c'est le programme qui va être compilé
- `-a` va créer un fichier de code source `prog_source.s` et un fichier exécutable `prog_source`
- `-m`est une option utilisée pour générer le code source en MSP 430
- `-l`permet de spécifier un autre chemin vers la liste des fonctions standards. Par défaut, le chemin est `compiler/lib_stdio.txt`

## Manuel de développeurs

Le code est codé en C++ et comporte les éléments suivant :

### Fichiers sources
- `ifcc.g4` contient la grammaire en format antlr4
- `main.cpp` contient le code C ++ pour appeler l'analyseur généré par antlr4 sur le nom de fichier fournit dans la ligne de commande.
- `visitor.cpp` est un visiteur de l'arbre d'analyse qui produit la représentation intermédiaire (avec un `visitor.h` associé)
- `symbolTable.cpp` est la table des symboles qui mémorise les correspondances entre un symbole et sa valeur/adresse (avec un `symbolTable.h` associé)
- `IR.cpp` est la représentation intermédiaire qui permettera la génération de code ASM x86 ou MSP430 (avec un `IR.h` associé)

### Fonctionnement du code
- Dans main.cpp on appelle la visite de l'axiome de notre grammaire
- Selon notre grammaire, l'axiome fait l'appel aux fonctions, au main et aux autres fonctions définies après. À son tour, le `main()` contient 
des appels d'instructions.
- Chaque fonction a son Control Flow Graph.
- Chaque visite d'une instruction met en mémoire et dans la table de symboles les variables déclarées, les constantes et les variables temporaires dont elle a besoin.
- Finalement, chaque fonction va appeler la méthode de génération de l'ASM et du MSP 430 (qui est dans IR.cpp).

Le Diagramme de Classe qui suit permet d'avoir une meilleur vue d'ensemble du projet.
![Diagramme de classe](./img/DDC.png)

### Scripts de compilation
- `Makefile` peut être utilisé pour compiler l'analyseur.** Attention** il faut changer les chemins des bibliothèques et des répertoires pour qu'ils soient coherents avec votre machine.
- `script-compile.sh` est un script qui compile le projet dans un environnement Ubuntu.
- `script-compile-bruno.sh` est un script qui compile le projet dans l'environnement de Bruno, c'est à dire avec un docker.
- `script-test.sh` est un script qui execute la batterie de tests dans un environnement Ubuntu.
- `script-test-bruno.sh` est un script qui execute la batterie de tests dans l'environnement de Bruno, c'est à dire avec un docker.

### Tests
Pour faire la batterie de tests il faut lancer le script `script-test.sh`avec la ligne de commande suivante. Avant de lancer les tests, pensez bien à compiler le compilateur.
```
sh script-test.sh
```


Dans le dossier `tests` nous retrouvons les éléments suivant :
- `pld-test.py` est le code python  appelé par le script `test-if.sh` qui permet de générer tous les fichiers des tests prédéfinis et de les comparer avec leur exécution avec `gcc` 
- `pld-wrapper.sh` est appelé par le code python `test-if.sh`
- `test-if.sh` est le script appelé par `script-test.sh` qui permet d'appeler le code python
- `test.sh` est le script appelé par `script-test-bruno.sh` qui permet d'appeler le code python dans l'environnement de Bruno, c'est à dire avec un docker.
 
Et deux dossiers :
- `tests` contient l'ensemble des codes `.c`qui vont être testés, c'est ici qu'il faut ajouter les nouveaux codes à tester. 
- `pld-test-output` contient l'ensemble des résultats obtenus après exécution des tests. Il y a les codes générés par `gcc`et ceux générés par notre compilateur.

        - `asm-xxx.s` contient le code assembleur généré par xxx
        - `msp430-xxx.s` contient le code assembleur MSP430 généré par xxx
        - `xxx-execute.txt` contient le `return` après exécution du code test par xxx
        - `xxx-compile.txt` contient le `return` après compilation du code test par xxx
        - `xxx-link.txt`
        - `input.c` contient le code du test`.c` en input
 
