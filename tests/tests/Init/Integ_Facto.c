int fact(int baseNumber) {
	int compteur = 1;
	int facto = 1;
	while(compteur < baseNumber) {
		facto = facto * compteur;
		compteur = compteur + 1;
	}
	return compteur;
}

int main() {
	return fact(8);
}
