int fibo(int i){
    int n = 1;
    int nprec = 0;
    int c = i-1;
    int res = 1;

    while (c>0) {
        res = nprec + n;
        nprec = n;
        n = res;
        c = c - 1;
    }

    return res;
}

int main(){
    return fibo(5);
}
